package com.example.demounittest.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;

@WebMvcTest(JamkrindoController.class)
@SpringJUnitConfig
public class JamkrindoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private HttpServletRequest httpServletRequest;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGreetingsWithValidName() throws Exception {
        // Arrange
        String name = "John";
        when(httpServletRequest.getParameter("name")).thenReturn(name);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/greetings")
                .param("name", name)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Sobat Jamkrindo John."));
    }

    @Test
    public void testGreetingsWithMissingName() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/greetings")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testGreetingsWithInvalidName() throws Exception {
        // Arrange
        String invalidName = "!@#$%^";
        when(httpServletRequest.getParameter("name")).thenReturn(invalidName);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/greetings")
                .param("name", invalidName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Sobat Jamkrindo !@#$%^."));
    }
}
