package com.example.demounittest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jamkrindo")
public class JamkrindoController {
    @GetMapping("/greetings")
    public ResponseEntity<String> greetings(
            @RequestParam("name")String name
    ){
        return new ResponseEntity<>(
                String.format("Hello Sobat Jamkrindo %s.",name),
                HttpStatus.OK
        );
    }

    // Create (POST)
    @PostMapping("/create")
    public ResponseEntity<String> createUser(@RequestParam("name") String name) {
        return new ResponseEntity<>("User created: " + name, HttpStatus.CREATED);
    }

    // Read (GET)
    @GetMapping("/user/{userId}")
    public ResponseEntity<String> getUser(@PathVariable("userId") String userId) {
        return new ResponseEntity<>("User details for ID: " + userId, HttpStatus.OK);
    }

    // Update (PUT)
    @PutMapping("/user/{userId}")
    public ResponseEntity<String> updateUser(
            @PathVariable("userId") String userId,
            @RequestParam("name") String newName) {
        return new ResponseEntity<>("User updated: " + newName, HttpStatus.OK);
    }

    // Delete (DELETE)
    @DeleteMapping("/user/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable("userId") String userId) {
        return new ResponseEntity<>("User deleted for ID: " + userId, HttpStatus.OK);
    }
}
